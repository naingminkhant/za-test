<?php

use App\Region;
use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = config('m-config.regions.name');

        foreach ($regions as $key=>$region)
        {
            Region::create([
                'name'  => $region
            ]);
        }
    }
}
