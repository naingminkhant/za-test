<?php

return [ 'name' =>
                    [
                        'Ayeyarwady',
                        'Bago',
                        'Magway',
                        'Mandalay',
                        'Sagaing',
                        'Yangon',
                        'Chin',
                        'Kachin',
                        'Kayah',
                        'Kayin',
                        'Mon',
                        'Rakhine',
                        'Shan'
                    ]
];