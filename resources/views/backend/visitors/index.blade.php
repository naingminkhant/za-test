@extends('layouts.app')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Visitors</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert" id="success-alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>{{ session('status') }}</strong>
                            </div>
                        @endif
                        <table class="table table-responsive table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Position [lat, long]</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($visitors as $visitor)
                                <tr>
                                    <td>{{ $visitor->id }}.</td>
                                    <td>{{ $visitor->name }}</td>
                                    <td>{{ $visitor->email }}</td>
                                    <td>{{ $visitor->phone_no }}</td>
                                    <td>
                                        {{ $visitor->no .', '. $visitor->street .', '. $visitor->quarter.', ' }}
                                        {{ $visitor->township->name .', '. $visitor->town->name .', '. $visitor->region->name}}
                                    </td>
                                    <td>
                                        {{ $visitor->latitude .', '. $visitor->longitude }}
                                    </td>
                                    <td>
                                        <a class="btn btn-primary" href="{{ route('visitor.edit', $visitor->id) }}">edit</a>
                                    </td>
                                    <td>
                                        {{ Form::open(['method' => 'DELETE','data-confirm' => 'delete?', 'route' => array('visitor.destroy', $visitor->id)]) }}
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure ?')">delete</button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
