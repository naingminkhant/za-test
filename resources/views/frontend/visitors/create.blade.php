@extends('frontend.layouts.app')
@section('content')
    {{ Form::open(array('name' => 'visitor')) }}
    <div class="form-group row  {{ $errors->has('name') ? ' error' : '' }}">
        <label for="name" class="col-2 col-form-label">Name</label>
        <div class="col-10">
            <input class="form-control" type="text" name="name" value="{{ old('name') }}">
            <small class="text-danger">{{ $errors->first('name') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('email') ? ' error' : '' }}">
        <label for="email" class="col-2 col-form-label">Email</label>
        <div class="col-10">
            <input class="form-control" type="email" name="email" value="{{ old('email') }}">
            <small class="text-danger">{{ $errors->first('email') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('phone_no') ? ' error' : '' }}">
        <label for="phone_no" class="col-2 col-form-label">Phone Number</label>
        <div class="col-10">
            <input class="form-control" type="tel" name="phone_no" value="{{ old('phone_no') }}">
            <small class="text-danger">{{ $errors->first('phone_no') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('no') ? ' error' : '' }}">
        <label for="no" class="col-2 col-form-label">No.</label>
        <div class="col-10">
            <input class="form-control" type="text" name="no" value="{{ old('no') }}">
            <small class="text-danger">{{ $errors->first('no') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('street') ? ' error' : '' }}">
        <label for="street" class="col-2 col-form-label">Street</label>
        <div class="col-10">
            <input class="form-control" type="text" name="street" value="{{ old('street') }}">
            <small class="text-danger">{{ $errors->first('street') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('quarter') ? ' error' : '' }}">
        <label for="quarter" class="col-2 col-form-label">Quarter</label>
        <div class="col-10">
            <input class="form-control" type="text" name="quarter" value="{{ old('quarter') }}">
            <small class="text-danger">{{ $errors->first('quarter') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('township') ? ' error' : '' }}">
        <label for="example-text-input" class="col-2 col-form-label">Township</label>
        <div class="col-10">
            <select class="select2-township form-control" name="township">
                @foreach($townships as $township)
                    <option value="{{ $township->id }}">{{ $township->name }}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('township') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('town') ? ' error' : '' }}">
        <label for="example-text-input" class="col-2 col-form-label">Town</label>
        <div class="col-10">
            <select class="select2-town form-control" name="town">
                @foreach($towns as $town)
                    <option value="{{ $town->id }}">{{ $town->name }}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('town') }}</small>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('region') ? ' error' : '' }}">
        <label for="example-text-input" class="col-2 col-form-label">Region</label>
        <div class="col-10">
            <select class="select2-region form-control" name="region">
                @foreach($regions as $region)
                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('region') }}</small>
        </div>
    </div>
    <div class="form-group offset-2">
        <fieldset class="gllpLatlonPicker">
            <div class="form-inline form-group">
                <input type="text" class="gllpSearchField form-control col-md-4">
                &nbsp; <input type="button" class="gllpSearchButton btn btn-default" value=" search">
            </div>
            <div class="gllpMap">Google Maps</div>
            <input type="hidden" class="gllpLatitude" value="20" name="latitude"/>
            <input type="hidden" class="gllpLongitude" value="20" name="longitude"/>
            <input type="hidden" class="gllpZoom" value="3"/>
        </fieldset>
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-success btn-lg">Submit</button>
    </div>
    {{ Form::close() }}
@endsection