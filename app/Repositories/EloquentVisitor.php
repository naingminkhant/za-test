<?php

namespace App\Repositories;

use App\Region;
use App\Town;
use App\Township;
use App\Visitor;
use Illuminate\Database\Eloquent\Model;

class EloquentVisitor implements VisitorRepository
{
    private $model;

    public function __construct($value = null)
    {
        $this->model = $value;
    }

    public function getRegion()
    {
       return $this->model->all();
    }

    public function getTown()
    {
        return $this->model->all();
    }

    public function getTownship()
    {
        return $this->model->all();
    }

    public function getVisitor()
    {
        return $this->model->all();
    }

    public function createTownship($attributes)
    {
        $data = Township::create([
            'name'  => $attributes
        ]);

        return $data->id;
    }

    public function createTown($attributes)
    {
        $data = Town::create([
            'name'  => $attributes
        ]);

        return $data->id;
    }

    public function createVisitor($model, array $attributes)
    {
        $model->name          = $attributes['name'];
        $model->email         = $attributes['email'];
        $model->phone_no      = $attributes['phone_no'];
        $model->no            = $attributes['no'];
        $model->street        = $attributes['street'];
        $model->quarter       = $attributes['quarter'];
        $model->township_id   = $attributes['township'];
        $model->town_id       = $attributes['town'];
        $model->region_id     = $attributes['region'];
        $model->latitude      = $attributes['latitude'];
        $model->longitude     = $attributes['longitude'];
        $model->save();
    }

    public function deleteVisitor($model)
    {
        $model->delete();
    }
}