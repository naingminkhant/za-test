<?php

namespace App\Repositories;

interface VisitorRepository
{

    function getTownship();

    function getTown();

    function getRegion();

    function getVisitor();

    function createTownship($attributes);

    function createTown($attributes);

    function createVisitor($model, array $attributes);

    function deleteVisitor($model);
}