<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table    = 'visitors';
    protected $fillable = [
                            'name',
                            'email',
                            'phone_no',
                            'no',
                            'street',
                            'quarter',
                            'township_id',
                            'town_id',
                            'region_id',
                            'latitude',
                            'longitude' ];

    public function township()
    {
        return $this->belongsTo('App\Township');
    }

    public function town()
    {
        return $this->belongsTo('App\Town');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function getTownshipName($id)
    {
        return Township::where('id', $id)->select('name')->first();
    }

    public function getTownName($id)
    {
        return Town::where('id', $id)->select('name')->first();
    }

    public function getRegionName($id)
    {
        return Region::where('id', $id)->select('name')->first();
    }
}
