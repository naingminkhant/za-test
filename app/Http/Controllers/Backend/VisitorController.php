<?php

namespace App\Http\Controllers\Backend;

use App\Visitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $visitors = Visitor::with('township','town','region')->get();

        return view('backend.visitors.index', compact('visitors'));
    }
}
