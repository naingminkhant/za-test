<?php

namespace App\Http\Controllers\Frontend;

use App\Town;
use App\Region;
use App\Visitor;
use App\Township;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EloquentVisitor;
use App\Http\Requests\CreateVisitorRequest;
use App\Http\Requests\UpdateVisitorRequest;

class VisitorController extends Controller
{

    private $repo;
    private $dir;

    public function __construct()
    {
        $this->repo = new EloquentVisitor();
        $this->dir  = 'frontend.visitors.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $townships  = $this->getAddress()['townships'];
        $towns      = $this->getAddress()['towns'];
        $regions    = $this->getAddress()['regions'];

        return view($this->dir.'create',compact('townships','towns','regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateVisitorRequest $request)
    {
        $request = $this->saveAddress($request->all());

        $visitor = new Visitor();

        $this->repo->createVisitor($visitor, $request);

        return redirect('/visitor')->with('status', 'Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $townships  = $this->getAddress()['townships'];
        $towns      = $this->getAddress()['towns'];
        $regions    = $this->getAddress()['regions'];

        $visitor = Visitor::findOrFail($id);

        return view($this->dir.'edit',compact('visitor', 'townships', 'towns','regions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVisitorRequest $request, $id)
    {
        $request = $this->saveAddress($request->all());

        $visitor = Visitor::findOrFail($id);

        $this->repo->createVisitor($visitor, $request);

        return redirect('/backend')->with('status', 'Successfully Updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visitor = Visitor::findOrFail($id);

        $this->repo->deleteVisitor($visitor);

        return redirect('/backend')->with('status', 'Successfully Deleted!');
    }


    private function getAddress()
    {
        $townships  = new EloquentVisitor(Township::all());
        $townships  = $townships->getTownship();

        $towns      = new EloquentVisitor(Town::all());
        $towns      = $towns->getTown();

        $regions    = new EloquentVisitor(Region::all());
        $regions    = $regions->getRegion();

        return ['townships' => $townships, 'towns' => $towns, 'regions' => $regions];
    }

    private function saveAddress($request)
    {
        if (!is_numeric($request['township']))
        {
           $request['township'] = $this->repo->createTownship($request['township']);
        }
        else
        {
            $request['township'] = $request['township'];
        }

        if (!is_numeric($request['town']))
        {
            $request['town'] = $this->repo->createTown($request['town']);
        }

        return $request;
    }
}
