<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateVisitorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:255',
            'email'     => 'required|email|unique:visitors',
            'phone_no'  => 'required',
            'no'        => 'required',
            'street'    => 'required',
            'quarter'   => 'required',
            'township'  => 'required',
            'town'      => 'required',
            'region'    => 'required'
        ];
    }
}
