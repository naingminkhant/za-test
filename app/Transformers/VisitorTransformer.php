<?php

namespace App\Transformers;

use App\Township;
use App\Visitor;
use League\Fractal\TransformerAbstract;

class VisitorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Visitor $visitor)
    {
        return [
            'id'        => $visitor->id,
            'name'      => $visitor->name,
            'email'     => $visitor->email,
            'phone'     => $visitor->phone_no,
            'address'   => $visitor->no .', '. $visitor->street .', '. $visitor->quarter,
            'township'  => $visitor->getTownshipName($visitor->township_id),
            'town'      => $visitor->getTownName($visitor->town_id),
            'region'    => $visitor->getRegionName($visitor->region_id)
        ];
    }
}
