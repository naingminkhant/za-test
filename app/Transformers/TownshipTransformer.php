<?php

namespace App\Transformers;

use App\Township;
use League\Fractal\TransformerAbstract;

class TownshipTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Township $township)
    {
        return [
            'name'  => $township->name
        ];
    }
}
